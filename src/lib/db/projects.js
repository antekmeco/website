export const projects = [
    {
        title: "CubeGame",
        description: "prosty sandbox 2d",
        image: "images/projects/cubegame.png",
    },
    {
        title: "Sexy Dmenu",
        description: "skrypt dmenu robiący sex i uruchamiający cubegame",
        image: "images/projects/sexyDmenu.png"
    },
    {
        title: "Moja strona",
        description: "strona na której jesteś",
        image: "images/projects/website.png"
    },
];